package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func main() {

	fName := os.Args[1]
	fmt.Println(strings.TrimSuffix(fName, filepath.Ext(fName)))
}
